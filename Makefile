all: dns_service.pb.go

dns_service.pb.go: 
	protoc -I api api/agent-api.proto --go_out=plugins=grpc:api
	GOOS=windows go build -o dns_agent.exe
	GOOS=linux go build -o dns_agent

clean:
	rm -f agent-api.pb.go
	rm -f dns_agent
	rm -f dns_agent.exe
