// +build linux

package main

import (
	"log"

	"gitlab.com/infraset/dns/agent/api"
)

type LnxDNSServer struct {}

func (srv *LnxDNSServer) ListRecords(zone string, rtype string) (records []api.Record, err error) {

	return
}

func (srv *LnxDNSServer) AddRecord(zone string, name string, rtype string, data string) (err error) {
	
	log.Printf("Adding record %s %s %s in zone %s", name, rtype, data, zone)

	return
}

func (srv *LnxDNSServer) DeleteRecord(zone string, name string, rtype string, data string) (err error) {
	
	log.Printf("Deleting record %s %s %s in zone %s", name, rtype, data, zone)

	return
}

func main() {

	server := api.NewServer(&LnxDNSServer{})
	
	err := server.Run(":50051")

	if err != nil {
		log.Printf("Failed to start agent server: %s\n", err.Error())
		return
	}
}