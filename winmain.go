// +build windows

package main

import (
	"log"
	"io"
	"io/ioutil"
	"bufio"
	"os/exec"
	"strings"
	"errors"

	"gitlab.com/infraset/dns/agent/api"
	"golang.org/x/sys/windows/svc"
	//"golang.org/x/sys/windows/svc/debug"
	//"golang.org/x/sys/windows/svc/eventlog"
)

type WinDNSServer struct {}

func (srv *WinDNSServer) ListRecords(zone string, rtype string) (records []api.Record, err error) {

	var output string

	output, err = srv.exec("dnscmd", "/ZonePrint", zone)

	if err != nil {
		return
	}

	records = make([]api.Record, 0)

	scanner := bufio.NewScanner(strings.NewReader(output))

	for scanner.Scan() {

		line := strings.Replace(scanner.Text(), "\t", " ", -1)
		parts := strings.Split(line, " ")

		if len(parts) < 4 || len(parts) > 5 {
			continue
		}

		var newRecord api.Record

		if len(parts) == 4 {
			newRecord = api.Record{Zone: zone, Name: parts[0], Type: parts[2], Data: parts[3]}
		} else {
			newRecord = api.Record{Zone: zone, Name: parts[0], Type: parts[3], Data: parts[4]}
		}

		if rtype == "" || rtype == "*" || rtype == newRecord.Type {
			records = append(records, newRecord)
		}
	}
	
	return
}

func (srv *WinDNSServer) AddRecord(zone string, name string, rtype string, data string) (err error) {

	var output string

	log.Printf("adding record %s %s %s to zone %s: ", name, rtype, data, zone)

	output, err = srv.exec("dnscmd", "/RecordAdd", zone, name, rtype, data)
	
	if err != nil {
		return
	}

	err = srv.textError(output)

	return
}

func (srv *WinDNSServer) DeleteRecord(zone string, name string, rtype string, data string) (err error) {

	var output string

	log.Printf("deleting record %s %s %s from zone %s: ", name, rtype, data, zone)

	output, err = srv.exec("dnscmd", "/RecordDelete", zone, name, rtype, data, "/f")

	if err != nil {
		return
	}

	err = srv.textError(output)
	
	return
}

func (srv *WinDNSServer) exec(command string, args...string) (output string, err error) {
	
	var outputReader io.ReadCloser
	var result []byte

	cmd := exec.Command(command, args...)

	outputReader, err = cmd.StdoutPipe()

	if err != nil {
		log.Printf("[WinDNSServer] failed to get stdout pipe: %v\n", err)
		return
	}

	err = cmd.Start()

	if err != nil {
		log.Printf("[WinDNSServer] failed to start command: %v\n", err)
		return
	}

	result, err = ioutil.ReadAll(outputReader)

	if err != nil {
		log.Printf("[WinDNSServer] failed to read output: %v\n", err)
		return
	}

	output = string(result)

	return
}

func (srv *WinDNSServer) textError(str string) (err error) {

	scanner := bufio.NewScanner(strings.NewReader(str))

	for scanner.Scan() {

		text := scanner.Text()

		if strings.Contains(text, "Command failed:") {
			parts := strings.Split(text, " ")

			for idx, p := range parts {
				log.Printf("p[%d] -> %s\n", idx, p)
			}

			err = errors.New(parts[3])
		}

	}

	return
}

const ListenAddr = "0.0.0.0:50051"

type WindowsService struct {
}

func (ws *WindowsService) Serve() {
}

func (ws *WindowsService) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (success bool, errno uint32) {

	const accept = svc.AcceptStop | svc.AcceptShutdown

	changes <- svc.Status{
		State: svc.Running,
		Accepts: accept,
	}

	go ServiceMain()

	for {

		c := <-r

		if c.Cmd == svc.Stop || c.Cmd == svc.Shutdown {
			break
		}
	} // end of loop

	panic(errors.New("stop requested"))

	return
}

func ServiceMain() {

	log.Printf("Listening at %s\n", ListenAddr)

	server := api.NewServer(&WinDNSServer{})

	err := server.Run(ListenAddr)

	if err != nil {
		panic(err)
	}
}

func runService(name string, isDebug bool) {
	
	err := svc.Run(name, &WindowsService{})

	if err != nil {
		return
	}
}

func main() {
	runService("dns_agent", false)
}