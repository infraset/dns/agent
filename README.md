-- DNS Record Service

--- Ferramentas Necessárias

Para compilação do projetos, as seguintes ferramentas são necessárias:

 1. protoc
 2. protoc-gen-go

 Abaixo seguem as instruções para instalação no linux:

```console
    sudo mkdir /opt/protoc && cd /opt/protoc
    sudo wget https://github.com/protocolbuffers/protobuf/releases/download/v3.11.4/protoc-3.11.4-linux-x86_64.zip
    sudo unzip protoc-3.11.4-linux-x86_64.zip
    sudo chmod +x bin/protoc
```

Adicione o diretório /opt/protoc/bin na variável de ambiente "PATH".

Em seguida instale o plugin para a linguagem Go:

```console
    go get -u google.golang.org/grpc
```
--- Instalação no Windows

Para instalar o agent como serviço, deve-se primeiramente copiar o executável para um diretório (ex: C:\dns-agent\dns_agent.exe)
e em seguida registrar o serviço através do seguinte comando:

```console
    sc create <nome do serviço> binPath=<caminho do executável>
```

Após a instalação e registro do serviço, deve-se iniciar o serviço com o comando abaixo:

```console
    sc start <nome do serviço>
```