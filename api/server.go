package api

import (
	"context"
	"net"

	"google.golang.org/grpc"
)

type ActionHandler interface {
	ListRecords(zone string, rtype string) ([]Record, error)
	AddRecord(zone string, name string, rtype string, data string) (err error)
	DeleteRecord(zone string, name string, rtype string, data string) (err error)
}

type Server struct {
	UnimplementedAgentServer
	handler ActionHandler
}

func NewServer(handler ActionHandler) *Server {
	return &Server{
		handler: handler,
	}
}

func (srv *Server) ListRecords(ctx context.Context, search *RecordSearch) (result *RecordList, err error) {

	var records []Record

	result = new(RecordList)

	records, err = srv.handler.ListRecords(search.Zone, search.Type)

	if err != nil {
		return
	}

	result.Items = make([]*Record, 0)

	for _, r := range records {
		result.Items = append(result.Items, &Record{
			Zone: r.Zone,
			Name: r.Name,
			Type: r.Type,
			Data: r.Data,
		})
	}

	return
}

func (srv *Server) AddRecord(ctx context.Context, record *Record) (result *AddRecordResult, err error) {

	result = new(AddRecordResult)

	err = srv.handler.AddRecord(record.Zone, record.Name, record.Type, record.Data)

	if err != nil {
		result.Success = false
		return
	}

	result.Success = true

	return
}

func (srv *Server) DeleteRecord(ctx context.Context, record *Record) (result *DeleteRecordResult, err error) {
	
	result = new(DeleteRecordResult)

	err = srv.handler.DeleteRecord(record.Zone, record.Name, record.Type, record.Data)

	if err != nil {
		result.Success = false
		return
	}

	result.Success = true

	return
}

func (srv *Server) Run(listenAt string) (err error) {

	lis, err := net.Listen("tcp", listenAt)
	
	if err != nil {
		return
	}

	s := grpc.NewServer()

	RegisterAgentServer(s, srv)

	err = s.Serve(lis)

	return
}