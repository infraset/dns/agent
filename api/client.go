package api

import (
	"context"
	"fmt"

	"google.golang.org/grpc"
)

type Client struct {
	ctx context.Context
	conn *grpc.ClientConn
	pbc AgentClient
}

func NewClient() (client *Client) {
	return &Client{
		ctx: context.Background(),
	}
}

func (cli *Client) Connect(address string) (err error) {

	cli.conn, err = grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())

	if err != nil {
		return
	}

	cli.pbc = NewAgentClient(cli.conn)



	return
}

func (cli *Client) Disconnect() {
	if cli.conn != nil {
		cli.conn.Close()
	}
}

func (cli *Client) ListRecords(zone string, rtype string) (records []Record, err error) {

	var list *RecordList

	list, err = cli.pbc.ListRecords(cli.ctx, &RecordSearch{
		Zone: zone,
		Name: "*",
		Type: rtype,
	})

	if err != nil {
		return
	}

	records = make([]Record, 0)

	for _, item := range list.Items {
		records = append(records, *item)
	}

	return
}

func (cli *Client) AddRecord(zone string, name string, rtype string, data string) (err error) {

	var result *AddRecordResult

	result, err = cli.pbc.AddRecord(cli.ctx, &Record{
		Zone: zone,
		Name: name,
		Type: rtype,
		Data: data,
	})

	if err != nil {
		return
	}

	if !result.Success {
		err = fmt.Errorf("failed to add record %s %s %s in zone %s", name, rtype, data, zone)
	}

	return
}

func (cli *Client) DeleteRecord(zone string, name string, rtype string, data string) (err error) {

	var result *DeleteRecordResult

	result, err = cli.pbc.DeleteRecord(cli.ctx, &Record{
		Zone: zone,
		Name: name,
		Type: rtype,
		Data: data,
	})

	if err != nil {
		return
	}

	if !result.Success {
		err = fmt.Errorf("failed to delete record %s %s %s from zone %s", name, rtype, data, zone)
	}

	return
}